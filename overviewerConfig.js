var overviewerConfig = {
    "CONST": {
        "tileSize": 384,
        "image": {
            "defaultMarker": "signpost.png",
            "signMarker": "signpost_icon.png",
            "bedMarker": "bed.png",
            "spawnMarker": "markers/marker_home.png",
            "spawnMarker2x": "markers/marker_home_2x.png",
            "queryMarker": "markers/marker_location.png",
            "queryMarker2x": "markers/marker_location_2x.png",
            "compass": {
                "0": "compass_upper-left.png",
                "1": "compass_upper-right.png",
                "3": "compass_lower-left.png",
                "2": "compass_lower-right.png"
            }
        },
        "mapDivId": "mcmap",
        "UPPERLEFT": 0,
        "UPPERRIGHT": 1,
        "LOWERLEFT": 3,
        "LOWERRIGHT": 2
    },
    "worlds": [
        "Mellinecarnft2020Jul - overworld",
        "Mellinecarnft2020Jul - nether",
        "Mellinecarnft2020Jul - end"
    ],
    "map": {
        "debug": false,
        "cacheTag": "1621648278",
        "north_direction": "lower-left",
        "controls": {
            "pan": true,
            "zoom": true,
            "spawn": true,
            "compass": true,
            "mapType": true,
            "overlays": true,
            "coordsBox": true
        }
    },
    "tilesets": [
        {
            "name": "Overworld NE",
            "zoomLevels": 14,
            "defaultZoom": 1,
            "maxZoom": 14,
            "path": "Overworld NE",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - overworld",
            "last_rendertime": 1621616169,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": [
                -160,
                99,
                -144
            ],
            "north_direction": 0
        },
        {
            "name": "Overworld NW",
            "zoomLevels": 14,
            "defaultZoom": 1,
            "maxZoom": 14,
            "path": "Overworld NW",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - overworld",
            "last_rendertime": 1621616169,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": [
                -160,
                99,
                -144
            ],
            "north_direction": 1
        },
        {
            "name": "Overworld SW",
            "zoomLevels": 14,
            "defaultZoom": 1,
            "maxZoom": 14,
            "path": "Overworld SW",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - overworld",
            "last_rendertime": 1621616169,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": [
                -160,
                99,
                -144
            ],
            "north_direction": 2
        },
        {
            "name": "Overworld SE",
            "zoomLevels": 14,
            "defaultZoom": 1,
            "maxZoom": 14,
            "path": "Overworld SE",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - overworld",
            "last_rendertime": 1621616169,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": [
                -160,
                99,
                -144
            ],
            "north_direction": 3
        },
        {
            "name": "Nether NE",
            "zoomLevels": 8,
            "defaultZoom": 1,
            "maxZoom": 8,
            "path": "Nether NE",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - nether",
            "last_rendertime": 1595979877,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 0
        },
        {
            "name": "Nether NW",
            "zoomLevels": 8,
            "defaultZoom": 1,
            "maxZoom": 8,
            "path": "Nether NW",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - nether",
            "last_rendertime": 1595979877,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 1
        },
        {
            "name": "Nether SW",
            "zoomLevels": 8,
            "defaultZoom": 1,
            "maxZoom": 8,
            "path": "Nether SW",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - nether",
            "last_rendertime": 1595979877,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 2
        },
        {
            "name": "Nether SE",
            "zoomLevels": 8,
            "defaultZoom": 1,
            "maxZoom": 8,
            "path": "Nether SE",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - nether",
            "last_rendertime": 1595979877,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 3
        },
        {
            "name": "End NE",
            "zoomLevels": 10,
            "defaultZoom": 1,
            "maxZoom": 10,
            "path": "End NE",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - end",
            "last_rendertime": 1595500695,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 0
        },
        {
            "name": "End NW",
            "zoomLevels": 10,
            "defaultZoom": 1,
            "maxZoom": 10,
            "path": "End NW",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - end",
            "last_rendertime": 1595500695,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 1
        },
        {
            "name": "End SW",
            "zoomLevels": 10,
            "defaultZoom": 1,
            "maxZoom": 10,
            "path": "End SW",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - end",
            "last_rendertime": 1595500695,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 2
        },
        {
            "name": "End SE",
            "zoomLevels": 10,
            "defaultZoom": 1,
            "maxZoom": 10,
            "path": "End SE",
            "base": "",
            "bgcolor": "#1a1a1a",
            "world": "Mellinecarnft2020Jul - end",
            "last_rendertime": 1595500695,
            "imgextension": "png",
            "isOverlay": false,
            "poititle": "Markers",
            "showlocationmarker": true,
            "center": [
                -160,
                99,
                -144
            ],
            "minZoom": 0,
            "spawn": false,
            "north_direction": 3
        }
    ]
};
